/*
Dieses Beispiel liefert grundsätzlich "authentication required" als
Ergebnis in der Route /oidc
*/

var { Issuer, Strategy } = require('openid-client');
const express= require('express');
const expressSesssion = require('express-session');
const passport = require('passport');

Issuer.discover('https://humboldtschule-berlin.eu/iserv/public/')
  .then(isrv => {
	console.log(isrv.grant_types_supported);
    var client = new isrv.Client({
      client_id: '',
      client_secret: '',
      redirect_uris: [ 'https://icebear.spdns.org/oidc' ],
	post_logout_redirect_uris: [ 'https://icebear.spdns.org/test/' ],
      token_endpoint_auth_method: 'authorization_code'
    });

    app=express();

app.use(
  expressSesssion({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true
  })
);

app.use(passport.initialize());
app.use(passport.session());

passport.use(
  'oidc',
  new Strategy({ client }, (tokenSet, userinfo, done) => {
    return done(null, tokenSet.claims());
  })
);

passport.serializeUser(function(user, done) {
  done(null, user);
});
passport.deserializeUser(function(user, done) {
  done(null, user);
});

// auth trigger
app.get('/auth', (req, res, next) => {
  passport.authenticate('oidc')(req, res, next);
});

// auth callback
app.get('/oidc', (req, res, next) => {
  console.log(req.url);
  passport.authenticate('oidc', function(err, user, info) {
        console.log("authenticate");
        console.log(err);
        console.log(user);
        console.log(info);
	})
/*{
    successRedirect: '/test/user',
    failureRedirect: '/test/'
  })*/
	(req, res, next);
	res.send('bla');
	res.end();
});

app.get('/user', (req,res,next) => {
	console.log(req.url);
	let out='<html><body>';
	for (const [key, value] of Object.entries(req.user)) {
  		out+=key+"="+ value+"<br>";
	}
	res.send(out+"</body></html>");
	res.end();
});

app.get('/', (req,res) => {
	res.send("<html><body>index</body></html");
});

app.listen(10101);
});
