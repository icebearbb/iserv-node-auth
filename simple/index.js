/*
Dieses Beispiel kann sich meines Achtens korrekt authentifizieren, liefert
aber keine Möglichkeit, Benutzerdaten (Name) abzufragen.
*/

var ClientOAuth2 = require('client-oauth2')
 
var iservAuth = new ClientOAuth2({
  clientId: '',
  clientSecret: '',
  accessTokenUri: 'https://humboldtschule-berlin.eu/iserv/oauth/v2/token',
  authorizationUri: 'https://humboldtschule-berlin.eu/iserv/oauth/v2/auth',
  redirectUri: 'https://icebear.spdns.org/oidc',
  scopes: ['profile', 'groups']
})

var express = require('express')
var app = express()
 
app.get('/auth', function (req, res) {
  var uri = iservAuth.code.getUri()
 
  res.redirect(uri)
})
 
app.get('/oidc', function (req, res) {
  iservAuth.code.getToken(req.originalUrl)
    .then(function (user) {
      console.log(user) //=> { accessToken: '...', tokenType: 'bearer', ... }
      
 
      // We should store the token into a database.
      return res.send(user.accessToken)
    })
})

app.listen(10101);
console.log("running...");
